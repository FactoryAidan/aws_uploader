require('dotenv').config();
const Uploader	=	require('../index.js');
const ignore_patterns	=	['.env','.git','node_modules'];

console.log('Setting Params');
Uploader.setParams({
	aws_s3_bucket_name		:	process.env.TEST_AWS_S3_BUCKET_NAME,
	aws_s3_region			:	process.env.TEST_AWS_S3_REGION,
	aws_access_key_id		:	process.env.TEST_AWS_ACCESS_KEY_ID,
	aws_secret_access_key	:	process.env.TEST_AWS_SECRET_ACCESS_KEY,
});

console.log('Truncating');
Uploader.truncate().then(U=>{
	console.log('Uploading');
	U.uploadZip('.',ignore_patterns);
});
