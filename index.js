const UploaderS3		=	new (require('./src/S3'));
const UploaderLambda	=	new (require('./src/Lambda'));

module.exports	=	{
	S3		:	UploaderS3,
	Lambda	:	UploaderLambda,
};
