# 🟢 Installation & Config

**npm**:

`npm install git+https://FactoryAidan@bitbucket.org/FactoryAidan/aws_uploader.git;`

**`.env` file**:
```
########################
# IAM User Credentials #
########################
AWS_ACCESS_KEY_ID=XXXXXXXXXXXXXXXXXXXX
AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

############
# DEFAULTS #
############
AWS_REGION=us-west-1

#############
# S3 Upload #
#############
AWS_S3_BUCKET_NAME=bucket_name_for_staging_env

```

# 🟢 `npm` CLI Usage

📚 The basic CLI functionalities are:

-	`truncate`
-	`upload`
-	`uploadZip`
-	`uploadLambdaFunction`
-	`uploadLambdaLayer`

⚙️ Put `scripts` in your project's `package.json` file:

`scripts` look very clean if you manage a complete `.env` file that doesn't need overrides

>	(*see `.env` sample above*):

You can be saved from having to define individual `scripts:{}` entries for each Lambda.
For `Lambda Functions/Layers`, we know what directory you're invoking `npm run` from.
Therefore, you can `CD` into a particular source directory and invoke `npm run` while only passing arguments that aren't implied by your directory location.
Arguments that can be implied are:

-	`function name`
-	`layer name`
-	`source_directory`

For Lambda Functions/Layers:

```
"scripts": {
	//	Simple
	"deploy-this_lambda_function":	"uploadToAWS --do=uploadLambdaFunction",
	"deploy-this_lambda_layer":		"uploadToAWS --do=uploadLambdaLayer",
	//	Advanaced
	"deploy-this_lambda_function":	"uploadToAWS --do=uploadLambdaFunction --ignore_patterns='aws-sdk'",
	"deploy-this_lambda_layer":		"uploadToAWS --do=uploadLambdaLayer --layer_name='test_layer'",	//	Arguments will override implied parameters like Lambda 'Layer Name'.
}
```

For `S3 Buckets`:

>	Things like static website deployments *(Gatsby for example)*

```
"scripts": {
	"deployStaging":		"npm run deleteStaging && npm run uploadStaging",
	"deleteStaging":		"uploadToAWS --do=truncateS3",
	"uploadStaging":		"uploadToAWS --do=uploadS3",

	"deployProduction":		"npm run deleteProduction && npm run uploadProduction",
	"deleteProduction":		"uploadToAWS --do=truncateS3 --bucket_name='com.production.env'",
	"uploadProduction":		"uploadToAWS --do=uploadS3 --bucket_name='com.production.env'",
	
	"uploadZip":			"uploadToAWS --do=uploadZipS3 --source_path='./src' --bucket_name='com.env.staging' --ignore_patterns='' --uploaded_zip_filename='package.zip'",
}
```

Use arguments to override things:

```
"scripts": {
	"deployStaging":		"npm run deleteStaging && npm run uploadStaging",
	"deleteStaging":		"uploadToAWS --do=truncateS3 --bucket_name='com.env.staging'",
	"uploadStaging":		"uploadToAWS --do=uploadS3 --source_path='./public' --bucket_name='com.env.staging'",

	"deployProduction":		"npm run deleteProduction && npm run uploadProduction",
	"deleteProduction":		"uploadToAWS --do=truncateS3 --bucket_name='com.env.produciton'",
	"uploadProduction":		"uploadToAWS --do=uploadS3 --source_path='./public' --bucket_name='com.env.production'",
}
```

>	(*see method documentation for complete list of parameters*):

⚙️ Now, from your CLI, you can run any of the following to manage your project in AWS S3:

-	`//	Staging`
-	`npm run deployStaging`
-	`npm run deleteStaging`
-	`npm run uploadStaging`
-	`//	Production`
-	`npm run deployProduction`
-	`npm run deleteProduction`
-	`npm run uploadProduction`
-	`//	Lambda`
-	`npm run deploy-this_lambda_function`
-	`npm run deploy-this_lambda_layer`

# 🟢 Programmatic Usage

## The Most Simple Usage

>	(*Doable if you have a complete `.env` file*)
```
//  Instantiate the package(s)
const Uploader	=	require('aws_uploader');


//	Delete then Upload
Uploader.S3.truncate().then(response=>{
	Uploader.S3.upload('.');
});


//	Delete, Zip your files, Upload
Uploader.S3.truncate().then(responses=>{
	Uploader.S3.uploadZip('./src').then(response=>{
	});
});

//	Update Lambda Function
Uploader.Lambda.uploadFunction().then(response=>{
	console.log('👌')
});
```

## Granular Usage
```
//	Instantiate the package
const Uploader	=	require('aws_uploader');


//	Sets instance parameters if you don't use a .env file.
Uploader.S3.setParams({
	aws_s3_bucket_name		:	'this_is_my_bucket'
	aws_region				:	'us-west-1',
	aws_access_key_id		:	'XXXXXXXXXXXXXXXXXXXX',
	aws_secret_access_key	:	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',	
});


//	Deletes all existing bucket content.
//	⚠️ This returns a Promise! It will not block execution while running. Use .then() if you need truncate() and then upload().
Uploader.S3.truncate();


//	Parameters used for upload
const source_path		=	'./public';	//	A relative path will work; relative to your script being run. Example: Gatsby builds final webroot files into the './public' directory.
const ignore_patterns	=	['.env','.git','node_modules'];	//	Excludes filenames matching your pattern(s).
const bucket_name		=	'overrides_bucketname_in_env';


//	Upload all folders/files in your source_path
Uploader.S3.upload(
	source_path,
	ignore_patterns,
	bucket_name
);


//	Upload a single .zip file containing all folders/files in your source_path
Uploader.S3.uploadZip(
	source_path,
	ignore_patterns,
	bucket_name,
	desired_uploadedzipfilename='package.zip',
);
```

## 🏅 Advanced Usage

```
//	--------
//	Sets config, empties the Bucket, zips your files, uploads to the Bucket
//	ℹ️ If you use a .env file, this is much cleaner; you wouldn't need to setParams() here.
//	--------
const Uploader	=	require('aws_s3_uploader');

Uploader.S3.setParams({
	aws_s3_bucket_name		:	'this_is_my_bucket'
	aws_region				:	'us-west-1',
	aws_access_key_id		:	'XXXXXXXXXXXXXXXXXXXX',
	aws_secret_access_key	:	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',	
}).truncate('target_bucket_name').then(response=>{

	//	Truncating is done. Upload fresh content.
	Uploader.S3.uploadZip(
		'./public',
		['.env'],
		'target_bucket_name',
		'package.zip',
	).then(responses=>{
		console.log('👌')
	});//uploaded

});//truncated


//	--------
//	Sets config, uploads to Lambda Function
//	--------
Updater.Lambda.setParams({
	aws_region				:	'us-west-1',
	aws_access_key_id		:	'XXXXXXXXXXXXXXXXXXXX',
	aws_secret_access_key	:	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',	
}).uploadFunction({
	source_directory	:	'.',
	function_name		:	'Your_Function_Name',
	ignore_patterns		:	['.env','.gitignore'],
}).then(response=>{
	console.log('👌')
});//updated

```

# 🟢 Method Documentation

### `Uploader.S3` Class 📑

**`.setParams(Parameters)`** 👇
```
/*	@description		Sets parameters on the instance
*	@param	{Object}	Parameters							{}
*	@param	{String}	Parameters.aws_s3_bucket_name		'my_bucket_name'
*	@param	{String}	Parameters.aws_s3_region			'us-west-2'
*	@param	{String}	Parameters.aws_access_key_id		'XXXXXXXXXXXXXXXXXXXX'
*	@param	{String}	Parameters.aws_secret_access_key	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
*	@return	{Object}	Instance of 'this'
*	@example			Uploader.setParams(Parameters).truncate().then(res=>console.log('👌'))
*/
```

---

**`.truncate(bucket_name)`** 👇
```
/*	@description		Deletes all existing bucket content.
*	@param	{String}	(Optional) bucket_name				'my_bucket_name'				The destination AWS bucket name.
*	@async				As a Promise, the response is .thenable() .
*	@return	{Promise}	Promise resolves to array of responses from AWS for each file batch deleted.
*	@example			Uploader.truncate().then(responses=>Uploader.upload('.'))
*/
```

---

**`.upload(source_path,ignore_patterns,bucket_name)`** 👇
```
/*	@description		Uploads files to a bucket.
*	@param	{String}	(Optional) source_path				'.'								Path to a directory. Can be absolute or relative to the script being run.
*	@param	{Array}		(Optional) ignore_patterns			['.env','.git','node_modules']	Is passed as comma-separated String if invoked from CLI.
*	@param	{String}	(Optional) bucket_name				'my_bucket_name'				The destination AWS bucket name.
*	@async				As a Promise, the response is .thenable() .
*	@example			Uploader.upload('.').then(res=>console.log('👌'))
*	@return	{Promise}	Promise resolves to array of responses from AWS for each file uploaded.
*/
```

---

**`.uploadZip(source_path,ignore_patterns,bucket_name,uploaded_zip_filename)`** 👇
```
/*	@description		Compresses source into a .zip and uploads that single .zip to a bucket.
*	@param	{String}	(Optional) source_path				'.'								Path to a directory. Can be absolute or relative to the script being run.
*	@param	{Array}		(Optional) ignore_patterns			['.env','.git','node_modules']	Is passed as comma-separated String if invoked from CLI.
*	@param	{String}	(Optional) bucket_name				'my_bucket'						The destination AWS bucket name.
*	@param	{String}	(Optional) uploaded_zip_filename	'package.zip'					The destination filename inside the AWS bucket.
*	@async				As a Promise, the response is .thenable() .
*	@example			Uploader.uploadZip('.').then(res=>console.log('👌'))
*	@return	{Promise}	Promise resolves to response from AWS for the .zip file uploaded.
*/
```

---

### `Updater.Lambda` Class 📑

**`.uploadFunction(Parameters)`** 👇
```
/*	@description		Deploys local source code to Lambda Function.
*	@param	{Object}	Parameters
*	@param	{String}	(Optional)Parameters.source_directory		'.'							The directory to upload to the Lamda Function
*	@param	{String}	(Optional)Parameters.function_name			'my_lambda_function'		The AWS Lambda function name.
*	@param	{Array}		(Optional)Parameters.ignore_patterns		['.env','.gitignore']		file/directory names to omit from the upload.
*	@async				As a Promise, the response is .thenable() .
*	@example			Updater.Lambda.uploadFunction({}).then(res=>console.log('👌'))
*	@return	{Promise}	Promise resolves to response from AWS.
*/
```

---

**`.uploadLayer(parameters)`** 👇
```
/*	@description		Deploys local source code Lambda Layer.
*	@param	{Object}	Parameters
*	@param	{String}	(Optional)Parameters.source_directory		'.'							The directory to upload to the Lamda Layer
*	@param	{String}	(Optional)Parameters.layer_name				'my_lambda_layer'			The AWS Lambda Layer name.
*	@param	{Array}		(Optional)Parameters.ignore_patterns		['.env','.gitignore']		file/directory names to omit from the upload.
*	@async				As a Promise, the response is .thenable() .
*	@example			Updater.Lambda.uploadLayer().then(res=>console.log('👌'))
*	@return	{Promise}	Promise resolves to response from AWS.
*/
```

---

# 🟢 Potential points of confusion

-	For your own safety/protection, the `ignore_patterns` argument has a default value of `['.DS_Store','.env','.git','node_modules']`. This means that you need to define that argument as `[]` when invoking the function if you want to include those in your upload.
-	In **S3**, the specified `--source_path` you pass will be left-trimmed from the uploaded `Object.key`.
-	To use the CLI, you must define your AWS credentials (& stuff) using a `.env` file.
-	You can pass the optional parameter, `bucket_name`, into methods. This will override whatever you defined in `.env` or with `Uploader.S3.setParams()`.
-	If you were using the `AWS Lambda` web-editor to manage your function's code and do not see the new code this npm package is deploying from `S3`, that means you have undeployed changes in the web-editor. Click 'Deploy' within AWS Lambda web-console so it is greyed-out, try to update with this npm package again, and refresh the web-console window to see an effect.

# 📚 Academic Reading

Resources:

-	[Create NPM CLI package](https://levelup.gitconnected.com/how-to-build-a-cli-npm-package-3ba98d6f9d4e)