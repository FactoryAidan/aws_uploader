module.exports	=	class {

	static real_cwd	=	{
		absolute	:	process.env.INIT_CWD,
		relative	:	`.${process.env.INIT_CWD.replace(process.cwd(),'')}`,
		basename	:	process.env.INIT_CWD.split('/').pop(),
	};

};
