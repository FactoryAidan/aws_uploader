const FS		=	require('fs');

function listRecursive(start_path='.',ignore_patterns){
	const file_list	=	[];
	FS.readdirSync(start_path).forEach(function(filename){

		//	Skip ignored filename strings.
		if( ignore_patterns.indexOf(filename) != -1 )	return;

		const pathfilename	=	[start_path,filename].join('/');
		const stat			=	FS.statSync(pathfilename);

	//	Deprecated:
	//	-	Was used to trim leading characters from path before pushing onto the file_list
	//		but this is now done by methods which digest the return value of this method.
	//	const pathfilename_relative	=	pathfilename.replace(/^\.\//,'');

		if( stat && stat.isDirectory() ){
			file_list.push( ...listRecursive(pathfilename,ignore_patterns) );
		}else{
			file_list.push(pathfilename);
		}//ifelse

	});//forEach
	return file_list;
}//func

module.exports	=	{
	listRecursive,
};

//	Reference(s)
//	-	https://stackoverflow.com/questions/5827612/node-js-fs-readdir-recursive-directory-search
