const FS				=	require('fs');

async function get( env_lambda_filename='./.env.lambda' ){
    const file_exists	=	await FS.existsSync(env_lambda_filename);

    //  Breakpoint
    if( !file_exists )	return;

    //  Proceed
    const contents  	=	await FS.readFileSync(env_lambda_filename);
    const lines		    =	contents.toString('utf8') .split('\n');

    //  Extract Variables
    const env_variables	=	{};
    lines.forEach(function(line){
        if( !line.trim() )	return;	//	If line is empty
        const [key,value]	        =	line.split('=');
        env_variables[key.trim()]	=	value.trim();
    });

    //  Return
    return env_variables;
}

module.exports = {
    get,
};
