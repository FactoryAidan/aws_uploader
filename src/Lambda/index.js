const AWS				=	require('aws-sdk');
const Parameters		=	require('./Parameters');
const UploaderS3		=	require('../S3');
const Paths				=	require('../Helpers/Paths');
const Env				=	require('./Env');

module.exports	=	class {

	defaults	=	{
		ignore_patterns		:	[
			'.DS_Store',
			'.env',
			'.git',
			'node_modules',
		],
		lambda	:	{
		//	Related to Layer
			compatible_runtimes	:	['nodejs12.x'],
			layer_name			:	Paths.real_cwd.basename,
		//	Related to Function
			function_name		:	Paths.real_cwd.basename,
		},
		source_directory			:	Paths.real_cwd.relative,
	};

//	------------
//	Set Settings
//	------------
	setParams(given_params={}){

		//	Instantiate Params
		this.params	=	new Parameters(given_params);

		//	Initialize S3 framework
		AWS.config.lambda	=	{
			accessKeyId		:	this.params.aws_access_key_id,
			secretAccessKey	:	this.params.aws_secret_access_key,
			region			:	this.params.aws_region,
		};

		this.Lambda	=	new AWS.Lambda();

		return this;
	}

//	-----------------
//	Validate Settings & Fallback Setting
//	-----------------
	_validateParameters(){

		//	Get new parameters if they don't exist.
		if( !this.params ){
			this.setParams();
		}
		//	Validate parameters
		this.params.validateParameters();

		return this;
	}

	uploadFunction(params){

		//	Defaults
		params.ignore_patterns	=	params.ignore_patterns	?? this.defaults.ignore_patterns;
		params.function_name	=	params.function_name 	?? this.defaults.lambda.function_name;
		params.source_directory	=	params.source_directory	?? this.defaults.source_directory;

		//
		console.log('Running with parameters:',params);

		//	Breakpoint / Fallback
		this._validateParameters();

		//	Zip
		const zip	=	(new UploaderS3)._createZip(
			params.source_directory,
			params.ignore_patterns,
		);

		//	Update
		const promises	=	[];
		promises.push(this._updateFunction({
			FunctionName	:	params.function_name,
			ZipFile			:	zip.toBuffer(),
		}));

		promises.push(this._updateFunctionConfiguration({
			FunctionName	:	params.function_name,
		}));

		return Promise.all(promises);

	}//method

	/*
	*	@param	{Object}	params					The Object having the following properties
	*	@param	{String}	params.FunctionName		The file contents as a Buffer
	*	@param	{String}	params.S3Bucket			The name of the AWS S3 Bucket
	*	@param	{String}	params.S3Key			The mime-type of the file
	*	@return	{Promise}							Resolves with the response object when the update is complete.
	*/
	_updateFunction(params){
		return new Promise(async (resolve,reject)=>{
			await this.Lambda.updateFunctionCode(params).promise()
				.then(async function(response){
				//	console.log('Response:',response);
				//	console.log(`Lambda Update: ${response.LastUpdateStatus}`);
					resolve(response);
				})
				.catch(error=>{
					console.log(Object.entries(error));
					reject(error);
				})
			;
		});//Promise
	}//method


	/*
	*	@param	{Object}	params							The Object having the following properties
	*	@param	{String}	params.FunctionName				The file contents as a Buffer
	*	@param	{Object}	params.Environment
	*	@param	{Object}	params.Environment.Variables	A Map of the environment variables as key:value
	*	@return	{Promise}									Resolves with the response object when the update is complete.
	*/
	_updateFunctionConfiguration(params){
		return new Promise(async (resolve,reject)=>{

			const env_variables	=	await Env.get();
			if( !env_variables )	return resolve();

			console.log("Updating Lambda's process.ENV with:",env_variables);

			params.Environment	=	{
				Variables	:	env_variables,
			};

			await this.Lambda.updateFunctionConfiguration(params).promise()
				.then(async function(response){
				//	console.log('Response:',response);
				//	console.log(`Lambda Update: ${response.LastUpdateStatus}`);
					resolve(response);
				})
				.catch(error=>{
					console.log(Object.entries(error));
					reject(error);
				})
			;

		});//Promise
	}//method


//	------
//	Layers
//	------
	uploadLayer(params){

		//	Defaults
		params.ignore_patterns	=	params.ignore_patterns	?? [] ;	//	Default to empty because Layers only contain packages you explicitly put into them; you wouldn't explicitly put something in and then ignore it.
		params.layer_name		=	params.layer_name 		?? this.defaults.lambda.layer_name;
		params.source_directory	=	params.source_directory	?? this.defaults.source_directory;

		//
		console.log('Running with parameters:',params);

		//	Breakpoint / Fallback
		this._validateParameters();

		//	Zip
		const zip	=	(new UploaderS3)._createZip(
			params.source_directory,
			params.ignore_patterns,
		);

		//	Update
		return this._updateLayer({
			CompatibleRuntimes	:	this.defaults.lambda.compatible_runtimes,
			LayerName			:	params.layer_name,
			Content				:	{
				ZipFile			:	zip.toBuffer(),
			},
		});

	}//method

	/*
	*	@param	{Object}	params						The Object having the following properties
	*	@param	{Array}		params.CompatibleRuntimes	[<Strings>] of runtimes. (nodejs10.x|nodejs12.x)
	*	@param	{String}	params.LayerName			The name of the AWS Lambda Layer
	*	@param	{Object}	params.Content				The Object having the following properties
	*	@param	{Buffer}	params.Content.ZipFile		The zipped package contents as a buffer.
	*	@return	{Promise}							Resolves with the response object when the update is complete.
	*/
	_updateLayer(params){
		return new Promise(async (resolve,reject)=>{
			await this.Lambda.publishLayerVersion(params).promise()
				.then(async function(response){
				//	console.log('Response:',response);
				//	console.log(`Lambda Update: ${response.LastUpdateStatus}`);
					resolve(response);
				})
				.catch(error=>{
					console.log(Object.entries(error));
					reject(error);
				})
			;
		});//Promise
	}//method

};