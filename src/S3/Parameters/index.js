require('dotenv').config();

module.exports	=	class Parameters {

	parameter_keys	=	[
		'aws_region',
		'aws_access_key_id',
		'aws_secret_access_key',
	];

	constructor(given_parameters={}){
		this.given_parameters	=	given_parameters;
		this.warnings	=	[];
		this.parameter_keys.forEach(this.assignParameterWithFallback.bind(this));
		if( this.warnings.length ){
			console.log("The following parameters are missing during initialization. They can be defined later by either passing 'params.the_parameter_name' or be defined in your project's '.env' file.");
			console.log(this.warnings);
		}
	}

	assignParameterWithFallback(parameter_name){
		this[parameter_name]	=	this.given_parameters[parameter_name];
		//	Try Fallback if needed.
		if( !this[parameter_name] ){
			this[parameter_name]	=	process.env[ parameter_name.toUpperCase() ];
			//	Detect missing definition.
			if( !this[parameter_name] ){
				this.warnings.push(`.env: ${parameter_name.toUpperCase()} (or params.${parameter_name}')`);
			}//if
		}//if
		return this;
	}

	validateParameters(){
		let are_parameters_valid	=	true;
		this.parameter_keys.forEach(parameter_name=>{
			if( !this[parameter_name] )	are_parameters_valid	=	false;
		});
		if( !are_parameters_valid )	throw new Error("There are missing parameters for AWS S3 upload. Look at the console output for warnings.")
		return are_parameters_valid;
	}

};//class