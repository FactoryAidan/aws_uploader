const FS				=	require('fs');
const AWS				=	require('aws-sdk');
const {listRecursive}	=	require(`../Helpers/Filesystem`);
const Parameters		=	require('./Parameters');
const AdmZip			=	require('adm-zip');
const mime				=	require('mime-types');

module.exports	=	class {

	defaults	=	{
		bucket_name			:	'',
		ignore_patterns		:	[
			'.DS_Store',
			'.env',
			'.git',
			'node_modules',
		],
		source_directory	:	'.',
		zip_filename		:	'package.zip',
	};


//	------------
//	Set Settings
//	------------
	setParams(given_params={}){

		//	Instantiate Params
		this.params	=	new Parameters(given_params);

		//	Initialize S3 framework
		AWS.config.s3	=	{
			accessKeyId		:	this.params.aws_access_key_id,
			secretAccessKey	:	this.params.aws_secret_access_key,
			region			:	this.params.aws_region,
		};

		this.S3	=	new AWS.S3();

		return this;
	}

//	-----------------
//	Validate Settings & Fallback Setting
//	-----------------
	_validateParameters(){

		//	Get new parameters if they don't exist.
		if( !this.params ){
			this.setParams();
		}
		//	Validate parameters
		this.params.validateParameters();

		return this;
	}

//	---------------
//	Delete Contents
//	-	Gets all Object Keys in Batches and deletes each batch.
//	---------------
	async truncate(bucket_name=this.defaults.bucket_name){

		//	Breakpoint / Fallback
		this._validateParameters();

		//	Default Value Assignment
		if( !bucket_name )	bucket_name	=	this.params.aws_s3_bucket_name;

		//	Array to contain Promises to delete every file batch listed by the bucket.
		const promisingDeletes	=	[];

		//	Starting Parameters - Properties are updated with each iteration.
		const function_params	=	{
				Bucket				:	bucket_name,
				ContinuationToken	:	null,
			//	MaxKeys				:	null,	//	1000 By Default
		};

		do {
			await this.S3.listObjectsV2(function_params).promise()
				.then(async (response)=>{
				//	console.log('Response:',response);
					function_params.ContinuationToken	=	response.NextContinuationToken;	//	This allows us to step through 'batches' or pages of results.

				//	Breakpoint - An API error is thrown by AWS if we send a delete request with nothing in it.
				if( !response.Contents.length ){
					console.log("Notice: The bucket was already empty when we tried to truncate its contents.");
					return;
				}

				//	--------------
				//	Delete Objects from this batch
				//	--------------
					const delete_these_objects	=	response.Contents.map(function(_object){return {Key:_object.Key};});
					const promiseToDelete	=	this.S3.deleteObjects({
						Bucket	:	function_params.Bucket,
						Delete	:	{
							Objects	:	delete_these_objects,
						//	Quiet	:	true,	//	Only reports when a deletion error occurs. If no error(s), an empty response is given.
						},
					}).promise()
						.then(response=>{
						//	console.log('Deletion Response:',response);
							console.log('🚮 Deleted item count in this batch:',response.Deleted.length);
							console.log(`👀 Errors from this batch: ${response.Errors.length?'Yes':'Nope'}`,response.Errors);
							return response;
						})
						.catch(error=>{
							console.log(Object.entries(error));
							return error;
						})
					;

					promisingDeletes.push( promiseToDelete );

				})
				.catch(error=>{
					console.log(Object.entries(error));
					function_params.ContinuationToken	=	null;
					reject(error);
				})
			;
		}while(function_params.ContinuationToken);

		return Promise.all(promisingDeletes);

	}//method


	/*
	*	@param	{Object}	upload_params				The Object having the following properties
	*	@param	{Buffer}	upload_params.Body			The file contents as a Buffer
	*	@param	{String}	upload_params.Bucket		The name of the AWS S3 Bucket
	*	@param	{String}	upload_params.ContentType	The mime-type of the file
	*	@param	{String}	upload_params.Key			The full path + filename as it will exist in AWS S3 .
	*	@return	{Promise}								Resolves with the response object when the upload is complete.
	*/
	_upload(upload_params){
		return new Promise(async (resolve,reject)=>{
			await this.S3.upload(upload_params).promise()
				.then(async function(response){
				//	console.log('Response:',response);
					console.log(`✅ ${response.Bucket} 👉 ${response.Key}`);
					resolve(response);
				})
				.catch(error=>{
					console.log(Object.entries(error));
					reject(error);
				})
			;
		});//Promise
	}//method


//	------------
//	Upload Files
//	------------
	upload(
		given_directory	=	this.defaults.source_directory,
		ignore_patterns	=	this.defaults.ignore_patterns,
		bucket_name		=	this.defaults.bucket_name,
	){

		//	Breakpoint / Fallback
		this._validateParameters();

		//	Default Value Assignment
		if( !bucket_name )	bucket_name	=	this.params.aws_s3_bucket_name;

		//	Get File List
		const file_list			=	listRecursive(given_directory,ignore_patterns);

		const escaped_given_directory	=	given_directory.replace(/\//g,'\\/').replace(/\./g,'\\.');
		const regex_pattern		=	new RegExp(`^${escaped_given_directory}\/`);	//	The pattern to left-trim the starting filepath from the upload key
		//	console.log('Ignoring Patterns:',ignore_patterns);	//	Debugging Only
		//	console.log('File List:',file_list);	//	Debugging Only

		//	Create a promise to upload for every file.
		const promisingUploads	=	[];
		file_list.forEach(pathfilename=>{
			promisingUploads.push(this._upload({
			//	ACL			:	'public-read',							//	String
				Body		:	FS.readFileSync(pathfilename),			//	Buffer - The file's content
				Bucket		:	bucket_name,							//	String
				ContentType	:	mime.lookup(pathfilename),				//	String
				Key			:	pathfilename.replace(regex_pattern,''),	//	String - left-trimmed pathfilename
			}));
		});//forEach

		return Promise.all(promisingUploads);

	}//method

//	------------------
//	Upload Zipped File
//	------------------
	_createZip(
		given_directory				=	this.defaults.source_directory,
		ignore_patterns				=	this.defaults.ignore_patterns,		
	){
		//	Get File List
		const file_list	=	listRecursive(given_directory,ignore_patterns);

		//	Left-Trim
		//	This is the RegularExpression pattern to left-trim the starting filepath from the upload key
		const escaped_given_directory	=	given_directory.replace(/\//g,'\\/').replace(/\./g,'\\.');
		const regex_pattern		=	new RegExp(`^${escaped_given_directory}\/`);

		//	Zip
		console.log('🗜 Compressing files into .zip');
		const zip		=	new AdmZip;
		file_list.forEach(function(pathfilename){
			const left_trimmed_pathfilename	=	pathfilename.replace(regex_pattern,'');
			const path_only					=	left_trimmed_pathfilename.split('/').slice(0,-1).join('/');
			zip.addLocalFile(pathfilename,path_only);
		});
		
		return zip;		
	}

	uploadZip(
		given_directory				=	this.defaults.source_directory,
		ignore_patterns				=	this.defaults.ignore_patterns,
		bucket_name					=	this.defaults.bucket_name,
		desired_uploadedzipfilename	=	this.defaults.zip_filename,
	){

		//	Breakpoint / Fallback
		this._validateParameters();

		//	Default Value Assignment
		if( !bucket_name )	bucket_name	=	this.params.aws_s3_bucket_name;

		//	Zip
		const zip	=	this._createZip(given_directory,ignore_patterns);

		//	Deliver
		console.log('🎬 Starting Upload...');
		return this._upload({
			Body		:	zip.toBuffer(),
			Bucket		:	bucket_name,
			ContentType	:	mime.lookup(desired_uploadedzipfilename),
			Key			:	desired_uploadedzipfilename,
		});

	}//method

};
