const yargs		=	require('yargs/yargs');
const {hideBin}	=	require('yargs/helpers');
const argv		=	yargs( hideBin(process.argv) ).argv;

const UploaderS3		=	new (require('../S3'));
const UploaderLambda	=	new (require('../Lambda'));

//	Defaults
const params	=	{
	bucket_name				:	argv.bucket_name ?? undefined,
	ignore_patterns			:	typeof argv.ignore_patterns == 'string' ? argv.ignore_patterns.split(',').filter(e=>e).map(e=>e.trim()) : undefined ,
	source_path				:	argv.source_path ?? undefined,
	uploaded_zip_filename	:	argv.uploaded_zip_filename ?? undefined,
	lambda_function_name	:	argv.function_name ?? undefined,
	lambda_layer_name		:	argv.layer_name ?? undefined,
};

switch( argv.do ){
	case 'truncateS3':
		console.log('🎬 Starting S3 Deletion...');
		UploaderS3.truncate(...[
			params.bucket_name,
		]).then(response=>{
			console.log('🏁 S3 Deletion Complete.');
		});
		break;
	case 'uploadS3':
		console.log('🎬 Starting S3 Upload...');
		UploaderS3.upload(...[
			params.source_path,
			params.ignore_patterns,
			params.bucket_name,
		]).then(response=>{
			console.log('🏁 S3 Upload Complete.');
		});
		break;
	case 'uploadZipS3':
		console.log('🎬 Starting S3 .zip Upload...');
		UploaderS3.uploadZip(...[
			params.source_path,
			params.ignore_patterns,
			params.bucket_name,
			params.uploaded_zip_filename,
		]).then(response=>{
			console.log('🏁 S3 Upload Complete.');
		});
/*
	case 'truncateThenUpload':
		uploaderS3.truncate(params.bucket_name).then(response=>{
			console.log('🎬 Starting Upload...');
			uploaderS3.upload(...[
				params.source_path,
				params.ignore_patterns,
				params.bucket_name,
			]).then(response=>{
				console.log('🏁 Upload Complete.');
			});
		});
		break;
	case 'truncateThenUploadZip':
		uploaderS3.truncate(params.bucket_name).then(response=>{
			uploaderS3.uploadZip(...[
				params.source_path,
				params.ignore_patterns,
				params.bucket_name,
				params.uploaded_zip_filename,
			]).then(response=>{
				console.log('🏁 Upload Complete.');
			});
		});
		break;
*/
	case 'uploadLambdaFunction':
		console.log('🎬 Starting Lambda Function Update...');
		UploaderLambda.uploadFunction({
			source_directory	:	params.source_path,
			function_name		:	params.lambda_function_name,
			ignore_patterns		:	params.ignore_patterns,
		}).then(response=>{
			console.log('🏁 Lambda Update Function Complete.');
		});
		break;
	case 'uploadLambdaLayer':
		console.log('🎬 Starting Lambda Layer Upload...');
		UploaderLambda.uploadLayer({
			source_directory	:	params.source_path,
			layer_name			:	params.lambda_layer_name,
			ignore_patterns		:	params.ignore_patterns,
		}).then(response=>{
			console.log('🏁 Lambda Layer Upload Complete.');
		});
		break;
	default:
		console.log("🚨 Whoops! Acceptable '--do' values are: truncateS3|uploadS3|uploadZipS3|uploadLambdaFunction|uploadLambdaLayer");
}//switch
